/* 
 * Copyright (C) 2006 Rob Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 */

#include <stdlib.h>
#include <string.h>

#include <glib-object.h>

#include "gnp-transform-funcs.h"

static void
transform_string_boolean (const GValue 	*src, 
			  GValue	*dst)
{
	const gchar *str;

	str = g_value_peek_pointer (src);
	if (str && *str) {
		if (str[0] == '0') {
			g_value_set_boolean (dst, FALSE);
		}
		else if (0 == strcasecmp ("false", str)) {
			g_value_set_boolean (dst, FALSE);
		}
		else {
			g_value_set_boolean (dst, TRUE);
		}
	}
	else {
		g_value_set_boolean (dst, FALSE);
	}
}

static void
transform_string_float (const GValue 	*src, 
			GValue		*dst)
{
	const gchar *str;

	str = g_value_peek_pointer (src);
	if (str && *str) {
		g_value_set_float (dst, atof (str));
	}
	else {
		g_value_set_float (dst, 0);
	}
}

static void
transform_string_int (const GValue 	*src, 
		      GValue		*dst)
{
	const gchar *str;

	str = g_value_peek_pointer (src);
	if (str && *str) {
		g_value_set_int (dst, atoi (str));
	}
	else {
		g_value_set_int (dst, 0);
	}
}

void
gnp_transform_funcs_init (void)
{
	if (!g_value_type_transformable (G_TYPE_STRING, G_TYPE_BOOLEAN)) {
		g_value_register_transform_func (G_TYPE_STRING, 
						 G_TYPE_BOOLEAN,
						 transform_string_boolean);
	}

	if (!g_value_type_transformable (G_TYPE_STRING, G_TYPE_FLOAT)) {
		g_value_register_transform_func (G_TYPE_STRING, 
						 G_TYPE_FLOAT,
						 transform_string_float);
	}

	if (!g_value_type_transformable (G_TYPE_STRING, G_TYPE_INT)) {
		g_value_register_transform_func (G_TYPE_STRING, 
						 G_TYPE_INT,
						 transform_string_int);
	}
}
