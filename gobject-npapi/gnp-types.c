/* 
 * Copyright (C) 2006 Rob Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 */

#include "config.h"
#include <string.h>
#include <gtk/gtk.h>
#include "gnp-types.h"

/* TODO Rob: fix this dirtyest imaginablable hacks that was done just for posing */
#if ENABLE_ABIWORD
#include "abiwidget.h"
#endif

typedef struct {
	const gchar 	*type_name;
	GType 		(* get_type) (void);
} _widgets_t;

static const _widgets_t _widgets[] = {
	{ "GtkAccelLabel", 		gtk_accel_label_get_type }, 
	{ "GtkAlignment", 		gtk_alignment_get_type },
	{ "GtkArrow", 			gtk_arrow_get_type },
	{ "GtkAspectFrame", 		gtk_aspect_frame_get_type },
	{ "GtkButton", 			gtk_button_get_type },
	{ "GtkCalendar", 		gtk_calendar_get_type },
	{ "GtkCheckButton", 		gtk_check_button_get_type },
	{ "GtkColorButton", 		gtk_color_button_get_type },
	{ "GtkColorSelection", 		gtk_color_selection_get_type },
	{ "GtkComboBox", 		gtk_combo_box_get_type },
	{ "GtkDrawingArea", 		gtk_drawing_area_get_type },
	{ "GtkEntry", 			gtk_entry_get_type },
	{ "GtkEventBox", 		gtk_event_box_get_type },
	{ "GtkExpander", 		gtk_expander_get_type },
	{ "GtkFileChooserButton", 	gtk_file_chooser_button_get_type },
	{ "GtkFileChooserWidget", 	gtk_file_chooser_widget_get_type },
	{ "GtkFontButton", 		gtk_font_button_get_type },
	{ "GtkFrame", 			gtk_frame_get_type },
	{ "GtkHBox", 			gtk_hbox_get_type },
	{ "GtkHButtonBox", 		gtk_hbutton_box_get_type },
	{ "GtkHPaned", 			gtk_hpaned_get_type },
	{ "GtkHRuler", 			gtk_hruler_get_type },
	{ "GtkHScrollbar", 		gtk_hscrollbar_get_type },
	{ "GtkHSeparator", 		gtk_hseparator_get_type },
	{ "GtkHandleBox", 		gtk_handle_box_get_type },
	{ "GtkIconView", 		gtk_icon_view_get_type },
	{ "GtkImage", 			gtk_image_get_type },
	{ "GtkInvisible", 		gtk_invisible_get_type },
	{ "GtkLabel", 			gtk_label_get_type },
	{ "GtkLayout", 			gtk_layout_get_type },
	{ "GtkMenuBar", 		gtk_menu_bar_get_type },
	{ "GtkNotebook", 		gtk_notebook_get_type },
/* huh? why fail?	{ "GtkOptionMenu", 		gtk_option_menu_get_type }, */
	{ "GtkPlug", 			gtk_plug_get_type },
	{ "GtkProgressBar", 		gtk_progress_bar_get_type },
	{ "GtkRadioButton", 		gtk_radio_button_get_type },
	{ "GtkRadioToolButton", 	gtk_radio_tool_button_get_type },
	{ "GtkScrolledWindow", 		gtk_scrolled_window_get_type },
	{ "GtkSeparator", 		gtk_separator_get_type },
	{ "GtkSocket", 			gtk_socket_get_type },
	{ "GtkSpinButton", 		gtk_spin_button_get_type },
	{ "GtkStatusbar", 		gtk_statusbar_get_type },
	{ "GtkTable", 			gtk_table_get_type },
	{ "GtkTextView", 		gtk_text_view_get_type },
	{ "GtkToggleButton", 		gtk_toggle_button_get_type },
	{ "GtkToggleToolButton", 	gtk_toggle_tool_button_get_type },
	{ "GtkToolButton", 		gtk_tool_button_get_type },
	{ "GtkToolbar", 		gtk_toolbar_get_type },
	{ "GtkTreeView", 		gtk_tree_view_get_type },
	{ "GtkVBox", 			gtk_vbox_get_type },
	{ "GtkVButtonBox", 		gtk_vbutton_box_get_type },
	{ "GtkVPaned", 			gtk_vpaned_get_type },
	{ "GtkVRuler", 			gtk_vruler_get_type },
	{ "GtkVScale", 			gtk_vscale_get_type },
	{ "GtkVScrollbar", 		gtk_vscrollbar_get_type },
	{ "GtkVSeparator", 		gtk_vseparator_get_type },
	{ "GtkViewport", 		gtk_viewport_get_type },

/* TODO Rob: fix this dirtyest imaginablable hacks that was done just for posing */
#if ENABLE_ABIWORD
	{ "AbiWidget", 		abi_widget_get_type },
#endif

	{ NULL, NULL } 
};

gchar *
gnp_types_get_mime_types (void)
{
	static gchar *mime_types = "\0";
	gchar *tmp;
	int i;

	i = 0;
	while (_widgets[i].type_name) {

		tmp = mime_types;
		mime_types = g_strconcat (tmp, GNP_TYPES_MIME_PREFIX, 
					 _widgets[i].type_name, 
					 ":gtk:Gtk+ Widgets;", NULL);
		if (tmp && *tmp) {
			free (tmp);
			tmp = NULL;
		}
		i++;
	}

	return mime_types;
}

GType 
gnp_types_lookup_type (const gchar *type_name)
{
	guint i;

	i = 0;
	while (_widgets[i].type_name) {
		if (0 == strcmp (type_name, _widgets[i].type_name)) {
			return _widgets[i].get_type ();
		}
		i++;
	}
	return 0;
}
