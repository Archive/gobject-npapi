/* 
 * Copyright (C) 2006 Rob Staudinger <robert.staudinger@gmail.com>
 *
 * Based on Gnome Chemisty Utils (moz-plugin.c)  
 * Copyright (C) 2002-2005 Jean Bréfort <jean.brefort@normalesup.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 */

#include <config.h>
	
#include <string.h>
#include <unistd.h>

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gdk/gdkx.h>

#include "npapi.h"
#include "npupp.h"

#include "gnp-transform-funcs.h"
#include "gnp-types.h"
#include "gnp-widget.h"

/* TODO Rob: fix this dirtyest imaginablable hacks that was done just for posing */
#if ENABLE_ABIWORD
#include <ap_Args.h>
#include <ap_UnixApp.h>
#include <abiwidget.h>
#endif

static NPNetscapeFuncs mozilla_funcs;

typedef struct {

	GnpWidget 	 widget;
	
	char 		*url;		/* The URL that this instance displays */
		
	int 		 width;		/* The size of the display area */
	int 		 height;	
	
	GdkNativeWindow	 xid;
	GdkWindow	*parent;
	GtkWidget 	*plug;
	
	NPP 		 instance;
} GtkNP;

static void
alert (GtkMessageType 	 type, 
       const char 	*str)
{
	GtkWidget *msg;

	msg = gtk_message_dialog_new (NULL, 
				      GTK_DIALOG_MODAL, type,
				      GTK_BUTTONS_CLOSE, str);
	gtk_dialog_run (GTK_DIALOG (msg));
	gtk_widget_destroy (msg);
	msg = NULL;
}

static NPError 
gtk_np_new (NPMIMEType	  mime_type, 
	    NPP 	  instance,
	    uint16 	  mode, 
	    uint16 	  argc, 
	    char 	**argn, 
	    char 	**argv,
	    NPSavedData  *saved)
{
	GtkNP 		*self;
	gboolean 	 is_param;
	int 		 i;

	if (instance == NULL) {
		return NPERR_INVALID_INSTANCE_ERROR;
	}
	
	instance->pdata = mozilla_funcs.memalloc (sizeof (GtkNP));
	self = (GtkNP *) instance->pdata;

	if (self == NULL) {
		return NPERR_OUT_OF_MEMORY_ERROR;
	}

	memset (self, 0, sizeof (GtkNP));

	self->instance = instance;
	
	/* create widget */
	gnp_widget_init (GNP_WIDGET (self), mime_type);
	if (!GNP_WIDGET (self)->widget) {
		g_warning ("Could not create widget `%s'", mime_type);
		return NPERR_INVALID_INSTANCE_ERROR;
	}

	/* set parameters */
	is_param = FALSE;
	i = 0;
	while (i < argc) {
		printf ("%s = '%s'\n", argn[i], argv[i]);
		if (!is_param && 
		    0 == strcmp ("PARAM", argn[i])) {
			is_param = TRUE;
		}
		if (is_param && argv[i]) {
			gnp_widget_set_param (GNP_WIDGET (self), argn[i], argv[i]);
		}
		i++;
	}

	return NPERR_NO_ERROR;
}

static NPError 
gtk_np_destroy (NPP 		  instance, 
		NPSavedData 	**save)
{
	GtkNP *self;

	if (instance == NULL) {
		return NPERR_INVALID_INSTANCE_ERROR;
	}
	
	self = (GtkNP *) instance->pdata;
	if (self == NULL) {
		return NPERR_INVALID_INSTANCE_ERROR;
	}

	mozilla_funcs.memfree (instance->pdata);
	instance->pdata = NULL;
	return NPERR_NO_ERROR;
}

gboolean
map_event_cb (GtkWidget *widget,
	GdkEvent  *event,
	GtkWidget *child)
{
	g_warning ("map-event");
/* TODO Rob: fix this dirtyest imaginablable hacks that was done just for posing */
#if ENABLE_ABIWORD
	if (IS_ABI_WIDGET (child)) {
		g_warning ("abi widget");
		assert (child);
		abi_widget_map_to_screen (ABI_WIDGET (child));

		g_object_set(G_OBJECT (child), "load-file", "test.abw");
	}
#endif	
	return FALSE;
}

gboolean
map_cb (GtkWidget *widget,
	GtkWidget *child)
{
	g_warning ("map");
/* TODO Rob: fix this dirtyest imaginablable hacks that was done just for posing */
#if ENABLE_ABIWORD
	if (IS_ABI_WIDGET (child)) {
		g_warning ("abi widget");
		assert (child);
		abi_widget_map_to_screen (ABI_WIDGET (child));
	}
#endif	
	return FALSE;
}

static NPError 
gtk_np_set_window (NPP		 instance, 
		   NPWindow	*window)
{
	GtkNP 		*self;
	int		 width;
	int		 height;

	self = (GtkNP *) instance->pdata;

	if ((GdkNativeWindow) window->window == self->xid) {
		// resize and redraw
		gdk_window_get_geometry (self->parent, NULL, NULL, 
					 &width, &height, NULL);
		gtk_window_resize (GTK_WINDOW (self->plug), width, height);
	} 
	else if (!self->plug) {

		if (!GNP_WIDGET (self)->widget) {
			return NPERR_INVALID_INSTANCE_ERROR;
		}

		self->xid = (GdkNativeWindow) window->window;
		self->parent = gdk_window_foreign_new (self->xid);
		self->plug = gtk_plug_new (self->xid);
		gdk_window_get_geometry (self->parent, NULL, NULL, 
					 &width, &height, NULL);
		gtk_window_set_default_size (GTK_WINDOW (self->plug), 
					     width, height);
		gtk_widget_realize (self->plug);
		XReparentWindow (GDK_WINDOW_XDISPLAY (self->plug->window), 
				 GDK_WINDOW_XID (self->plug->window), 
				 self->xid, 0, 0);
		XMapWindow (GDK_WINDOW_XDISPLAY (self->plug->window), 
			    GDK_WINDOW_XID (self->plug->window));

		gtk_container_add (GTK_CONTAINER (self->plug), GNP_WIDGET (self)->widget);
		g_signal_connect (G_OBJECT (self->plug), "map-event", G_CALLBACK (map_event_cb), GNP_WIDGET (self)->widget);
//		g_signal_connect (G_OBJECT (self->plug), "map", G_CALLBACK (map_cb), GNP_WIDGET (self)->widget);
		gtk_widget_show_all (self->plug);		
	}
	else {
		// reparent ?
		g_assert_not_reached ();
	}

	return NPERR_NO_ERROR;
}

static NPError 
gtk_np_new_stream (NPP		 instance, 
		   NPMIMEType 	 type, 
		   NPStream 	*stream,
		   NPBool 	 seekable, 
		   uint16 	*stype)
{
	GtkNP *self;

	if (instance == NULL) {
		return NPERR_INVALID_INSTANCE_ERROR;
	}
	
	self = (GtkNP *) instance->pdata;
	if (self == NULL) {
		return NPERR_NO_ERROR;
	}
	
	*stype = NP_ASFILEONLY;

	return NPERR_NO_ERROR;
}

static void 
gtk_np_print (NPP	 instance, 
	      NPPrint 	*platformPrint)
{
	/* TODO implement */
}

static void 
gtk_np_stream_as_file (NPP		 instance, 
		       NPStream 	*stream, 
		       const char 	*fname)
{
	/* TODO implement */
}

NPError 
NP_GetValue (void 		*future, 
	     NPPVariable 	 variable, 
	     void 		*value)
{
	switch (variable) {
	case NPPVpluginNameString:
		*((char **) value) = "gtk-np";
		break;
	case NPPVpluginDescriptionString:
		*((char **) value) =
			"gtk-np-"VERSION". "
			"Gtk Widgets for Mozilla.";
		break;
	case NPPVpluginScriptableNPObject:
		alert (GTK_MESSAGE_INFO, __FUNCTION__);
		/* fallthru */
	default:
		return NPERR_GENERIC_ERROR;
	}
	return NPERR_NO_ERROR;
}

char *
NP_GetMIMEDescription (void)
{
	static gchar *mime_types = NULL;

	if (mime_types) {
		return mime_types;
	}

	mime_types = gnp_types_get_mime_types ();

	return mime_types;
}



/* TODO Rob: fix this dirtyest imaginablable hacks that was done just for posing */
#if ENABLE_ABIWORD
static AP_UnixApp *_abiword_app = NULL;
#endif

/* This is called to initialise the plugin
 */
NPError 
NP_Initialize (NPNetscapeFuncs 	*moz_funcs, 
	       NPPluginFuncs 	*plugin_funcs) 
{
/* TODO Rob: fix this dirtyest imaginablable hacks that was done just for posing */
#if ENABLE_ABIWORD
	XAP_Args XArgs = XAP_Args(PACKAGE);
	_abiword_app = new AP_UnixApp(&XArgs, PACKAGE);
	AP_Args Args = AP_Args(&XArgs, PACKAGE, _abiword_app);
	Args.parsePoptOpts();
#endif

	if (moz_funcs == NULL || plugin_funcs == NULL) {
		return NPERR_INVALID_FUNCTABLE_ERROR;
	}
	if ((moz_funcs->version >> 8) > NP_VERSION_MAJOR) {
		return NPERR_INCOMPATIBLE_VERSION_ERROR;
	}
	if (moz_funcs->size < sizeof (NPNetscapeFuncs)) {
		return NPERR_INVALID_FUNCTABLE_ERROR;
	}
	if (plugin_funcs->size < sizeof (NPPluginFuncs)) {
		return NPERR_INVALID_FUNCTABLE_ERROR;
	}

	memcpy (&mozilla_funcs, moz_funcs, sizeof (NPNetscapeFuncs));
	
	plugin_funcs->version		= (NP_VERSION_MAJOR << 8) + NP_VERSION_MINOR;
	plugin_funcs->size		= sizeof (NPPluginFuncs);
	plugin_funcs->newp		= NewNPP_NewProc (gtk_np_new);
	plugin_funcs->destroy		= NewNPP_DestroyProc (gtk_np_destroy);
	plugin_funcs->setwindow		= NewNPP_SetWindowProc (gtk_np_set_window);
	plugin_funcs->newstream		= NewNPP_NewStreamProc (gtk_np_new_stream); 
	plugin_funcs->destroystream 	= NULL;
	plugin_funcs->asfile		= NewNPP_StreamAsFileProc (gtk_np_stream_as_file); 
	plugin_funcs->writeready	= NULL;
	plugin_funcs->write		= NULL;
	plugin_funcs->print		= NewNPP_PrintProc (gtk_np_print);
	plugin_funcs->urlnotify		= NULL;
	plugin_funcs->event		= NULL;
#ifdef OJI
	plugin_funcs->javaClass		= NULL;
#endif

	/* internal initialisation */
	gnp_transform_funcs_init ();

	return NPERR_NO_ERROR;
}

NPError	
NP_Shutdown (void)
{
	/* TODO clean up */
	return NPERR_NO_ERROR;
}

NPError	
NPP_GetValue (NPP 		 instance, 
	      NPPVariable 	 variable, 
	      void 		*ret_alue)
{
	/* TODO not called yet */
alert (GTK_MESSAGE_INFO, __FUNCTION__);
	return NPERR_NO_ERROR;
}
