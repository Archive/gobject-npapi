/* 
 * Copyright (C) 2006 Rob Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 */

/* TODO this will be the wrapper for gtk widgets */

#ifndef GNP_WIDGET_H
#define GNP_WIDGET_H

#include <gtk/gtk.h>

#define GNP_WIDGET(obj) ((GnpWidget *) obj)

G_BEGIN_DECLS

typedef struct _GnpWidget GnpWidget;

struct _GnpWidget {
	GtkWidget *widget;
};

void gnp_widget_init 	 (GnpWidget *self, const gchar *mime_type);
void gnp_widget_set_param (GnpWidget *self, const gchar *name, const gchar *value);

G_END_DECLS

#endif /* GNP_WIDGET_H */
