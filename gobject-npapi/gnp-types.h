/* 
 * Copyright (C) 2006 Rob Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 */

#ifndef GNP_WIDGETS_H
#define GNP_WIDGETS_H

#include <glib.h>
#include <glib-object.h>

#define GNP_TYPES_MIME_PREFIX "widget/x-"

G_BEGIN_DECLS

gchar * gnp_types_get_mime_types (void);
GType 	gnp_types_lookup_type 	 (const gchar *type_name);

G_END_DECLS

#endif /* GNP_WIDGETS_H */
