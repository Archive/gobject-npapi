/* 
 * Copyright (C) 2006 Rob Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 */

/* TODO this will be the wrapper for gtk widgets */

#include <string.h>

#include "gnp-widget.h"
#include "gnp-types.h"

void
gnp_widget_init (GnpWidget 	*self, 
		 const gchar 	*mime_type)
{
	GType type;

	type = gnp_types_lookup_type (mime_type + strlen (GNP_TYPES_MIME_PREFIX));
	self->widget = (GtkWidget *) g_object_new (type, NULL);
}

void
gnp_widget_set_param (GnpWidget 	*self, 
		      const gchar 	*name, 
		      const gchar 	*value)
{
	GValue val = {0, };

	g_value_init (&val, G_TYPE_STRING);
	g_value_set_string (&val, value);
	g_object_set_property (G_OBJECT (self->widget), name, &val);
}
