# Copyright (C) 2000-2004 Marco Pesenti Gritti
# Copyright (C) 2003, 2004, 2005, 2006 Christian Persch
# Copyright (C) 2006 Rob Staudinger
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

AC_INIT([gobject-npapi],[0.1],[http://bugzilla.gnome.org/enter_bug.cgi?product=gobject-npapi],[gobject-npapi])

GNOME_COMMON_INIT

AC_PREREQ([2.59])

AC_CONFIG_MACRO_DIR([m4])
AC_CONFIG_HEADERS([config.h])
AC_CONFIG_SRCDIR([configure.in])

AM_INIT_AUTOMAKE([1.9 foreign dist-bzip2 no-dist-gzip])

AM_MAINTAINER_MODE

AC_ENABLE_SHARED([yes])
AC_ENABLE_STATIC([no])

AC_LIBTOOL_DLOPEN
AC_PROG_LIBTOOL

AC_ISC_POSIX
AC_PROG_CC
AC_PROG_CXX
AM_PROG_CC_STDC
AC_HEADER_STDC
AC_PATH_PROG([GLIB_GENMARSHAL], [glib-genmarshal])
AC_PATH_PROG([GLIB_MKENUMS],[glib-mkenums])

PKG_PROG_PKG_CONFIG

GNOME_DEBUG_CHECK
GNOME_COMPILE_WARNINGS([maximum])
GNOME_CXX_WARNINGS

MORE_WARN_FLAGS=
DEPRECATION_FLAGS=
MOZILLA_WARN_CXXFLAGS="-Wno-ctor-dtor-privacy -Wno-non-virtual-dtor"

if test "x$enable_maintainer_mode" = "xyes"; then
	AC_DEFINE([MAINTAINER_MODE],[1],[Define to enable 'maintainer-only' behaviour])
	enable_debug=yes
        DEPRECATION_FLAGS="-DG_DISABLE_DEPRECATED -DGDK_DISABLE_DEPRECATED -DGDK_PIXBUF_DISABLE_DEPRECATED -DGCONF_DISABLE_DEPRECATED -DGNOME_VFS_DISABLE_DEPRECATED -DLIBGLADE_DISABLE_DEPRECATED -DPANGO_DISABLE_DEPRECATED -DGTK_DISABLE_DEPRECATED -DGNOME_DISABLE_DEPRECATED"
	MOZILLA_WARN_CXXFLAGS="-Wall -Wconversion -Wpointer-arith -Wcast-align -Woverloaded-virtual -Wsynth $MOZILLA_WARN_CXXFLAGS"
fi

GLIB_REQUIRED=2.8.0
GTK_REQUIRED=2.8.3

gnp_dependencies="
	glib-2.0 >= $GLIB_REQUIRED 	\
	gmodule-2.0 			\
	gtk+-2.0 >= $GTK_REQUIRED"

abi_pkg='abiword-2.5'
AC_ARG_ENABLE([abiword], 
[AS_HELP_STRING([--enable-abiword], [build with abiword support])],
[
	if test "x$enableval" = "xyes"; then
		gnp_dependencies="
			$gnp_dependencies	\
			$abi_pkg"
		AC_DEFINE(ENABLE_ABIWORD, [1], [include support for abiword])
	fi
])

PKG_CHECK_MODULES([DEPENDENCIES], [ $gnp_dependencies ])

if test "x$enable_abiword" = "xyes"; then
# HACK to include all the stuff we need.
# Just so all the current whizzbang is not stopped by ever build breaking Rob.
AC_MSG_CHECKING([additional abiword includes])
abi_base_includedir=`pkg-config --variable=includedir $abi_pkg`"/$abi_pkg"
abi_sub_includes=" \
 -I ${abi_base_includedir}/ev/xp \
 -I ${abi_base_includedir}/ev/unix \
 -I ${abi_base_includedir}/gr/xp \
 -I ${abi_base_includedir}/gr/unix \
 -I ${abi_base_includedir}/util/xp \
 -I ${abi_base_includedir}/util/unix \
 -I ${abi_base_includedir}/xap/xp \
 -I ${abi_base_includedir}/xap/unix \
 -I ${abi_base_includedir}/text/fmt/xp \
 -I ${abi_base_includedir}/text/ptbl/xp \
 -I ${abi_base_includedir}/spell/xp \
 -I ${abi_base_includedir}/ap/xp \
 -I ${abi_base_includedir}/ap/unix \
 -I ${abi_base_includedir}/impexp/xp \
 -I ${abi_base_includedir}/impexp/unix"
DEPENDENCIES_CFLAGS="${DEPENDENCIES_CFLAGS} ${abi_sub_includes}"
fi

AC_SUBST([DEPENDENCIES_CFLAGS])
AC_SUBST([DEPENDENCIES_LIBS])

# *******
# Mozilla
# *******

GECKO_INIT([MOZILLA])

AC_SUBST([MOZILLA])
AC_SUBST([MOZILLA_FLAVOUR])
AC_SUBST([MOZILLA_INCLUDE_ROOT])
AC_SUBST([MOZILLA_HOME])
AC_SUBST([MOZILLA_PREFIX])
AC_SUBST([MOZILLA_LIBDIR])
AC_SUBST([MOZILLA_EXTRA_LIBS])

case "$MOZILLA" in
mozilla) 	min_version=1.7.9 ;;
seamonkey) 	min_version=1.0 ;;
*firefox) 	min_version=1.0.5 ;;
*thunderbird) 	min_version=1.0.5 ;;
xulrunner) 	min_version=1.8 ;;
esac

PKG_CHECK_MODULES([GECKO],[gtk+-2.0 ${gecko_cv_gecko}-xpcom >= $min_version $gecko_cv_extra_pkg_dependencies])
AC_SUBST([GECKO_CFLAGS])
AC_SUBST([GECKO_LIBS])

# *******************************
# Add warning flags
# *******************************

AM_CPPFLAGS="$AM_CPPFLAGS $DEPRECATION_FLAGS"
AM_CFLAGS="$AM_CFLAGS $WARN_CFLAGS $MORE_WARN_FLAGS"
AM_CXXFLAGS="$AM_CXXFLAGS $WARN_CXXFLAGS $MOZILLA_WARN_CXXFLAGS"
AC_SUBST([AM_CPPFLAGS])
AC_SUBST([AM_CFLAGS])
AC_SUBST([AM_CXXFLAGS])
AC_SUBST([AM_LDFLAGS])

# *****************
# API Documentation
# *****************

dnl check for python
AM_PATH_PYTHON(2.3)
AM_CHECK_PYTHON_HEADERS(,[AC_MSG_ERROR(could not find Python headers)])

dnl check for pygtk
PKG_CHECK_MODULES(PYGTK, pygtk-2.0)
AC_SUBST(PYGTK_CFLAGS)
AC_PATH_PROG(PYGTK_CODEGEN, pygtk-codegen-2.0, no)
if test "x$PYGTK_CODEGEN" = xno; then
  AC_MSG_ERROR(could not find pygtk-codegen-2.0 script)
fi

AC_MSG_CHECKING(for pygtk defs)
PYGTK_DEFSDIR=`$PKG_CONFIG --variable=defsdir pygtk-2.0`
AC_SUBST(PYGTK_DEFSDIR)
AC_MSG_RESULT($PYGTK_DEFSDIR)

AC_MSG_CHECKING(for gnome-python argtypes dir)
GNOME_PYTHON_ARGTYPES_DIR=`$PKG_CONFIG --variable=argtypesdir gnome-python-2.0`
AC_SUBST(GNOME_PYTHON_ARGTYPES_DIR)
AC_MSG_RESULT($GNOME_PYTHON_ARGTYPES_DIR)

# ************
# Output files
# ************

AC_CONFIG_FILES([
Makefile
gobject-npapi/Makefile
tests/Makefile
],
[])

AC_OUTPUT

echo "
    Configuration
        abiword support:          ${enable_abiword}
"
